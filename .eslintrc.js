module.exports = {
  "extends": [
    "google",
    "plugin:react/recommended"
  ],
  "plugins": [
    "promise",
    "react"
  ],
  "rules": {
    "require-jsdoc": "off",
    "react/display-name": "off",
  },
  "parser": "babel-eslint"
};
