'use strict';

class User {
  constructor(client) {
    this.api = client;
  }
  login(email, password) {
    return this.api.fetch('POST', 'user/login', false, {
      email: email,
      password: password,
    });
  }
  signup(email, password) {
    return this.api.fetch('POST', 'user', false, {
      email: email,
      password: password,
    });
  }
  get() {
    return this.api.fetch('GET', 'user', true);
  }
  activate(token) {
    return this.api.fetch('POST', 'user/activate/' + token, false);
  }
  reset(token, password) {
    return this.api.fetch('POST', 'user/reset/' + token, false, {
      password: password,
    });
  }
  sendResetMail(email) {
    return this.api.fetch('POST', 'user/reset_mail', false, {
      email: email,
    });
  }
  subscription() {
    return this.api.fetch('GET', 'user/subscription', true);
  }
  unsubscribe() {
    return this.api.fetch('DELETE', 'user/subscription', true);
  }
  sendActivationMail() {
    return this.api.fetch('POST', 'user/activate_mail', true);
  }
  changeEmail(password, newEmail) {
    return this.api.fetch('POST', 'user/email', true, {
      password: password,
      newEmail: newEmail,
    });
  }
  changePassword(password, newPassword) {
    return this.api.fetch('POST', 'user/password', true, {
      password: password,
      newPassword: newPassword,
    });
  }
  addCard(token) {
    return this.api.fetch('POST', 'user/card', true, token);
  }
  getCard() {
    return this.api.fetch('GET', 'user/card', true);
  }
  deleteCard() {
    return this.api.fetch('DELETE', 'user/card', true);
  }
  setVatIn(vatIn) {
    return this.api.fetch('POST', 'user/vatin', true, {'vatIn': vatIn});
  }
}

class Tokens {
  constructor(client) {
    this.api = client;
  }
  create() {
    return this.api.fetch('POST', 'tokens', true);
  }
  get() {
    return this.api.fetch('GET', 'tokens', true);
  }
  delete(id) {
    return this.api.fetch('DELETE', 'tokens/' + id, true);
  }
}

class Plans {
  constructor(client) {
    this.api = client;
  }
  get() {
    return this.api.fetch('GET', 'plans', false);
  }
  subscribe(plan, stripeToken) {
    return this.api.fetch('POST', 'plan/' + plan + '/subscribe', true, {
      token: stripeToken,
    });
  }
  getVatRate(country) {
    return this.api.fetch('GET', 'plan/tax/'+country, true);
  }
}

export default class Client {
  constructor(url) {
    if (url.slice(-1) != '/') {
      url = url + '/';
    }
    this.url = url;
    this.headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    };
    this.User = new User(this);
    this.Tokens = new Tokens(this);
    this.Plans = new Plans(this);
    this.authToken = localStorage.getItem('authToken') || '';
    this.LoggedIn = this.authToken != '';
  }
  setAuthToken(token) {
    localStorage.setItem('authToken', token);
    this.authToken = token;
    this.LoggedIn = true;
  }
  removeAuthToken() {
    localStorage.removeItem('authToken');
    this.authToken = '';
    this.LoggedIn = false;
  }
  fetch(method, path, sendToken, body) {
    let headers = this.headers;
    if (sendToken) {
      headers['Authorization'] = 'Bearer ' + this.authToken;
    }
    return fetch(this.url + path, {
      method: method,
      headers: headers,
      body: JSON.stringify(body),
    })
    .then((resp) => {
      if (resp.ok) {
        return resp.json();
      }
      return resp.json().then((json) => {
        let error = new Error(json.message || response.statusText);
        error.statusCode = resp.status;
        if (resp.status == 403 && json.message == 'Invalid JWT.') {
          error.JWTInvalid = true;
          this.authToken = null;
          this.LoggedIn = false;
        }
        throw error;
      });
    });
  }
}
