import React from 'react';
import PropTypes from 'prop-types';
import StripeCheckout from 'react-stripe-checkout';
import config from 'config';

import Tooltip from './tooltip';

const tooltipDeleteCardDisabled = <p>You need to unsubscribe before deleting
  the credit card</p>;

export default class Billing extends React.Component {
  constructor(props) {
    super(props);
    this.state = {vatID: this.props.user.vatin};
    this.handleChangeVatID = this.handleChangeVatID.bind(this);
    this.setVatID = this.setVatID.bind(this);
  }
  handleChangeVatID(e) {
    this.setState({
      vatID: e.target.value,
    });
  }
  setVatID(e) {
    e.preventDefault();
    this.props.api.User.setVatIn(this.state.vatID)
    .then(() => {
      this.props.update();
    }).catch(this.props.loadFailed);
  }
  render() {
    if (!this.props.card) {
      return <div id="billing" className="wrapper">
        <div className="center cta">
          <p>Add a credit card<br />to subscribe to plans.</p>
          <StripeCheckout
            token={this.props.addCard}
            billingAddress={true}
            email={this.props.user.email}
            stripeKey={config.stripeKey}
            name="Latency.at"
            panelLabel="Add"
            description="Add Credit Card to subscribe to plans.">
            <button className="button-primary">Add Credit Card</button>
          </StripeCheckout>
        </div>
      </div>;
    }

    let card = this.props.card;
    let buttonDelete;
    console.log(this.props.subscription);
    if (this.props.subscription.plan.id != 'free') {
      buttonDelete = <Tooltip tip={tooltipDeleteCardDisabled}>
        <button onClick={this.props.deleteCard} disabled>
          Delete card
        </button></Tooltip>;
    } else {
      buttonDelete = <button onClick={this.props.deleteCard}>
        Delete Card
      </button>;
    };

    let vatNote;
    let vatIDClass = this.state.vatID.length > 0 ? 'control-label-up' : '';
    // Prompt EU people to enter VAT
    if (this.props.inEU) {
      let vatIDStatus;
      if (!this.props.user.vatin) {
        vatIDStatus = <span>According to your credit card, you are located
        in {card.country}. We automatically apply the {this.props.vatRate*100}%
        VAT rate to your monthly bill. As a business customer you can add your
        VAT identification number below to remove VAT from your bill.</span>;
      }

      vatNote = <div>
        <form onSubmit={this.setVatID}>
          <div className="form-group">
            <label htmlFor="vatID" className={'control-label ' + vatIDClass}>
            VAT ID</label>
            <input id="vatID" type="text" placeholder="VAT ID"
              style={{margin: '1em 0'}}
              value={this.state.vatID}
              onChange={this.handleChangeVatID} />
            <a href="#" className="button-nude" onClick={this.setVatID}>Save</a>
          </div>
        </form>
        <p style={{fontSize: '16px'}}>
          {vatIDStatus}
        </p>
      </div>;
    }

    const cardName = card.name + ', xxxx-' + card.last4;
    return <div id="billing" className="wrapper">
      <h4>{cardName}</h4>
      <p>Country {card.country}, Expiring {card.exp_month}/{card.exp_year}</p>
      {vatNote}
      <div className="button-bar" style={{marginTop: '1em'}}>
        <StripeCheckout
          token={this.props.addCard}
          billingAddress={true}
          email={this.props.user.email}
          stripeKey={config.stripeKey}
          name="Latency.at"
          panelLabel="Replace"
          description="Replace Credit Card.">
          <button className="button-primary" style={{marginRight: '1em'}}>
            Replace Card</button>
        </StripeCheckout>
        {buttonDelete}
      </div>
    </div>;
  }
};

Billing.propTypes = {
  api: PropTypes.object,

  user: PropTypes.object,
  inEU: PropTypes.bool,
  card: PropTypes.object,
  subscription: PropTypes.object,
  vatRate: PropTypes.number,

  addCard: PropTypes.func,
  deleteCard: PropTypes.func,
  loadFailed: PropTypes.func,

  update: PropTypes.func,
};
