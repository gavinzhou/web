import React from 'react';
import PropTypes from 'prop-types';

export default class ChangeEmail extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			password: '',
      email: '',
      email2: '',
		};
    this.handleSubmitEmailChange =
      this.handleSubmitEmailChange.bind(this);
	}
  handleSubmitEmailChange(e) {
    e.preventDefault();
    if (this.state.email != this.state.email2) {
      this.props.notify('Email and repeated Email do not match', 'warning');
      return;
    }
    this.props.api.User.changeEmail(this.state.password, this.state.email)
      .then(this.props.notify('Check your mail to confirm address change',
      'warning'))
      .catch((err) => this.props.notify(err.message, 'danger'));
  }
  classFor(obj) {
    return 'control-label ' + (obj.length > 0 ? 'control-label-up' : '');
  }
  render() {
    return (
      <form onSubmit={this.handleSubmitEmailChange}>
        <div className="form-group">
          <label htmlFor="password"
            className={this.classFor(this.state.password)}>
            Current Password
          </label>
          <input type="password" id="password" placeholder="Current Password"
            onChange={(e) => this.setState({password: e.target.value})} />
        </div>
        <div className="form-group">
          <label htmlFor="email"
            className={this.classFor(this.state.email)}>
            New Email Address
          </label>
          <input type="email" id="email" placeholder="New Email" onChange={
            (e) => this.setState({email: e.target.value})} />
        </div>
        <div className="form-group">
          <label htmlFor="email2"
            className={this.classFor(this.state.email2)}>
            Repeat New Email Address
          </label>
          <input type="email" id="email2" placeholder="Repeat New Email"
            onChange={(e) => this.setState({email2: e.target.value})} />
        </div>
        <button className="button-primary" type="submit">Change Email</button>
      </form>
    );
  }
}

ChangeEmail.propTypes = {
  api: PropTypes.object.isRequired,
  notify: PropTypes.func.isRequired,
};
