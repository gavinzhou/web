import React from 'react';
import PropTypes from 'prop-types';

export default class ChangePassword extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			password: '',
			newPassword: '',
			newPassword2: '',
		};
    this.handleSubmitPasswordChange =
      this.handleSubmitPasswordChange.bind(this);
	}
  handleSubmitPasswordChange(e) {
    e.preventDefault();
    if (this.state.newPassword != this.state.newPassword2) {
      this.props.notify('New Password and repeated, new Password do not match',
        'warning');
      return;
    }
    this.props.api.User.changePassword(
      this.state.password,
      this.state.newPassword
    )
      .then(() => this.props.notify('Password changed', 'success'))
      .catch((err) => this.props.notify(err.message, 'danger'));
  }
  classFor(obj) {
    return 'control-label ' + (obj.length > 0 ? 'control-label-up' : '');
  }
  render() {
    return (
      <form onSubmit={this.handleSubmitPasswordChange}>
        <div className="form-group">
          <label htmlFor="password"
            className={this.classFor(this.state.password)}>
            Current Password
          </label>
          <input type="password" id="password" placeholder="Current Password"
            onChange={(e) => this.setState({password: e.target.value})} />
        </div>

        <div className="form-group">
          <label htmlFor="newPassword"
            className={this.classFor(this.state.newPassword)}>
            New Password
          </label>
          <input type="password" id="newPassword" placeholder="New Password"
            onChange={(e) => this.setState({newPassword: e.target.value})} />
        </div>

        <div className="form-group">
          <label htmlFor="newPassword2"
            className={this.classFor(this.state.newPassword2)}>
            Repeat New Password
          </label>
          <input type="password" id="newPassword2"
            placeholder="Repeat New Password" onChange={(e) =>
              this.setState({newPassword2: e.target.value})} />
        </div>
        <button className="button-primary" type="submit">Change Password
          </button>
      </form>
    );
  }
}

ChangePassword.propTypes = {
  api: PropTypes.object,
  notify: PropTypes.func,
};

