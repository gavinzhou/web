DataDog already supports retrieving metrics from Prometheus endpoints for some
of their checks. While they are [working on generic
support](https://github.com/DataDog/dd-agent/pull/3317#issuecomment-346875945),
currently a custom check is needed.
For this we wrote
[datadog-agent-prometheus](https://github.com/latency-at/datadog-agent-prometheus)
which allows you to consume arbitrary Prometheus metrics in DataDog.

Once installed, you can retrieve Latency.at metrics into DataDog with a config
like this:

```
init_config:
  
instances:
  - target: https://sfo1.do.mon.latency.at/probe?target=https://latency.at
    config: &config
      headers: &headers
        Authorization: "Bearer your-token"
      drop:
        - probe_.*
      keep:
        - probe_http_.*
  - target: https://nyc1.do.mon.latency.at/probe?target=https://latency.at
    config: *config
  - target: https://fra1.do.mon.latency.at/probe?target=https://latency.at
    config: *config
```
Please note that each datadog-agent with this check enabled sends requests to Latency.at.
