While we don't support graphite directly, you can use
[Telegraf](https://github.com/influxdata/telegraf) to pull our metrics and send
it to your graphite server.

This minimal config uses the [Prometheus Input
Plugin](https://github.com/influxdata/telegraf/tree/master/plugins/inputs/prometheus)
to collect Latency.at metrics and send them to graphite:

```
[agent]
  interval = "10s"

[[inputs.prometheus]]
  urls = [
    "https://nyc1.do.mon.latency.at/probe?target=https://latency.at",
    "https://sfo1.do.mon.latency.at/probe?target=https://latency.at",
    "https://fra1.do.mon.latency.at/probe?target=https://latency.at",
    "https://sgp1.do.mon.latency.at/probe?target=https://latency.at",
    "https://blr1.do.mon.latency.at/probe?target=https://latency.at"
  ]
  bearer_token = "/latency-token"

[[outputs.graphite]]
  servers = ["192.168.1.102:2003"]
  # template = "host.tags.measurement.field"
```

Make sure the file `bearer_token` points to includes your auth token without
trailing newline.

You can uncomment the template and adjust it to reformat the resulting metric.

Need native support? [Let us know!](mailto:sales@latency.at)
