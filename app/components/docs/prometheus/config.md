#### Configuration

The Probes are HTTP services that provide Prometheus metrics about a given
target. They are reachable via HTTP and HTTPS under this name:

```
https://<region>.<provider>.mon.latency.at
http://<region>.<provider>.mon.latency.at:8080
```

To request metrics from the Probes, the following header must be set:

```
Authorization: Bearer Your-Token
```

Prometheus does that for you when setting the
[bearer_token](https://prometheus.io/docs/operating/configuration/#scrape_config)
config option. You can create and revoke tokens in the Dashboard.

A minimal scrape config might look like this:

```
  - job_name: 'latency-at-simple'
    bearer_token: 'your-auth-token'
    metrics_path: /probe
    params:
      module: [http_2xx]
      target: [latency.at]
    static_configs:
      - targets:
        - https://fra1.do.mon.latency.at
```

Since duplicating this for every site and exporter can get tedious, it's
recommended to use [relabeling](https://prometheus.io/docs/operating/configuration/#<relabel_config>)
to generate the necessary configuration from a simple list of targets in the
format *site@region.provider*.

You can also get metrics about your account by scraping
`https://api.latency.at/api/metrics` with your token like this:

```
  - job_name: 'latency-at-account'
    bearer_token: 'your-auth-token'
    scheme: https
    metrics_path: /api/metrics
    static_configs:
      - targets:
        - api.latency.at
```

This give you the following metrics:

**`lat_account_available_requests`:** Remaining requests for this month

**`lat_plan_renewal_timestamp_seconds`:** UNIX timestamp of next plan renewal

**`lat_plan_requests`:** In plan included number of requests

You can use the following configuration generator. Add the targets you want to
monitor to the module you want to use. If you want to send a HTTP GET request
and expect a 2xx response, enter the target in the `http_2xx` field. After that
you can download the generated configuration.
