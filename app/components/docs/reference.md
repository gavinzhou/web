### Endpoints
The Probes are HTTP services that provide Prometheus metrics about a given
target. They are reachable via HTTP and HTTPS under this name:

```
https://<region>.<provider>.mon.latency.at/probe
http://<region>.<provider>.mon.latency.at:8080/probe
```

### Parameters
The `probe` endpoint supports two parameters:

- `target`:
  - URL or address of the site or service to get metrics for
- `module`; The check module to use:
  - `http_2xx`: Sends GET, expects 2xx
  - `http_post_2xx`: Sends POST, expects 2xx
  - `tcp_connect`: Connects via TCP, expects established connection
  - `pop3s_banner`: Connects via TCP+TLS, expects "^+OK" response
  - `ssh_banner`: Connects via TCP, expects "^SSH-2.0-" response
  - `icmp`: Sends ICMP echo request (ping), expects response

### Authentication
To request metrics from the Probes, the following header must be set:

```
Authorization: Bearer Your-Token
```

### Available Metrics
#### Probe metrics
<br />
<table>
  <thead><tr><td>Type</td><td>Name</td><td>Description</td></tr></thead> 
  <tbody>
    <tr><td>*</td><td>probe_success</td><td>
      1 if request was successful</td></tr>
    <tr><td>*</td><td>probe_duration</td><td>
      Duration of the request in seconds</td></tr>
    <tr><td>*</td><td>probe_ip_protocol</td><td>
      IP protocol version used (4 or 6)</td></tr>
    <tr><td>http</td><td>probe_http_duration_seconds</td><td>
      Duration of http request by phase, summed across all redirects
      (if any)</td></tr>
    <tr><td>http</td><td>probe_http_status_code</td><td>
      Status Code of response</td></tr>
    <tr><td>http</td><td>probe_http_redirects</td><td>
      Number of redirects followed during request</td></tr>
    <tr><td>http</td><td>probe_content_length</td><td>
      Content length in response header</td></tr>
    <tr><td>http(s)</td><td>probe_http_ssl</td><td>
      1 if HTTPS is used</td></tr>
    <tr><td>https</td><td>probe_ssl_earliest_cert_expiry</td><td>
      Timestamp of earliest expiring certificate</td></tr>
  </tbody>
</table>

#### Account metrics
Additionally to the performance and availability metrics
You can also get metrics about your account by scraping
`https://api.latency.at/api/metrics`

This give you the following metrics:

**`lat_account_available_requests`:** Remaining requests for this month

**`lat_plan_renewal_timestamp_seconds`:** UNIX timestamp of next plan renewal

**`lat_plan_requests`:** In plan included number of requests

You can use the following configuration generator. Add the targets you want to
monitor to the module you want to use. If you want to send a HTTP GET request
and expect a 2xx response, enter the target in the `http_2xx` field. After that
you can download the generated configuration.
