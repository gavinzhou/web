[Zabbix's User parameters](https://www.zabbix.com/documentation/3.0/manual/config/items/userparameters)
allows you to integrate Latency.at metrics into your Zabbix setup.

### Simple Availability Monitoring
If you're just interested in the availability of a target, you can use this
custom check definition:

```
UserParameter=latency.probe[*],echo 'Authorization: Bearer your-token'|curl -H@-
"https://$2.do.mon.latency.at/probe?target=$1'|grep -c 'probe_success 1'
```

And use it like this to get the availability for a target in a given
region:

```
latency.probe[https%3A%2F%2Fapi.latency.at,nyc1.do]
```
<aside>Note: The parameters needs to be [URL encoded](https://en.wikipedia.org/wiki/Percent-encoding).</aside>

<br />

### Full integration of metrics
Zabbix 3.4 added support for [dependent
items](https://www.zabbix.com/documentation/3.4/manual/config/items/itemtypes/dependent_items)
which allows to configure User Parameter and derive multiple metrics from it,
allowing to fully integrate Latency.at.

Add this UserParameter to the [Zabbix agent
config](https://www.zabbix.com/documentation/3.4/manual/appendix/config/zabbix_agentd)
to retrieve metrics from Latency.at:

```
UserParameter=latency[*],echo 'Authorization: Bearer your-token' | curl -H@- "https://$2.mon.latency.at/probe?target=$1"
```

Next create a parent item for each Target/Region combination you are interested
in. For that go to Configuration -> Hosts -> Zabbix Server -> Items and click
"Create item":

![Parent Item Creation](zabbix/zabbix-parent-item.png)

 - Specify a name: *latency-api-nyc1-do*
 - Create a key with parameters specifying the region and target, refering the
  parent item created in the previous step: `latency[https%3A%2F%2Fapi.latency.at,nyc1.do]`
 - Set `Type of information` to `Text`

<aside>Note: The parameters needs to be [URL encoded](https://en.wikipedia.org/wiki/Percent-encoding).</aside>


Finally a *Dependent Item* needs to be created for each individual metrics that
you want to integrate. For that, search for "latency-api-nyc1-do" to show the
newly created item:

![Search for Parent Item](zabbix/zabbix-search.png)

Click on "..." and select "Create Dependent Item":

![Search for Parent Item](zabbix/zabbix-item.png)

- Specify a name for this metric: *NYC API: Connect Duration*
- Specify a key for the new item: *latency.api.nyc.duration*
- Set unit to Seconds

Now click Preprocessing and the `add` text link to create a new step:

![Preprocessing Metric](zabbix/zabbix-preprocessing.png)

- Set Name to `Regular expression`
- Set Parameters to `/^probe_http_duration_seconds{phase="connect"} (.*)/`
- Set the field after Parameters to `\1`

Repeat this for each individual metric you are interested in.

For more details, see the [official Zabbix
documentation](https://www.zabbix.com/documentation/3.4/manual/config/items/itemtypes/dependent_items).
