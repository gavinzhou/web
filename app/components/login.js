import React from 'react';
import PropTypes from 'prop-types';
import {browserHistory} from 'react-router';

// Need to disable this because https://github.com/eslint/eslint/issues/1534
// eslint-disable-next-line no-unused-vars
import {validatePassword, validateEmail} from './validations';

import Alert from './alert';

import {Link} from 'react-router';

export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      view: props.view || 'login',
      termsAccepted: false,
      email: '',
      email_label_class: '',
      password: '',
      password_label_class: '',
      alertBottom: '100%',
    };
    this.views = {
      login: {
        loginClass: 'active',
        submitText: 'Login',
        introText: 'Log into your Account',
        note: <Link to='/login/reset' className="button-nude">
          Forgot your password?</Link>,
      },
      reset: {
        submitText: 'Reset',
        introText: 'Reset your password',
        note: <div style={{margin: '1em 0'}}>
          We will send you a link to reset your password.</div>,
      },
      signup: {
        signupClass: 'active',
        submitText: 'Signup',
        introText: 'Sign up to start your free trial',
        note: <label className="checkbox">
          <input type="checkbox" onChange={(e) => {
            this.setState({termsAccepted: e.target.checked});
          }} />
          I've read and agree to the&nbsp;
          <Link to="/legal/terms">terms and conditions.</Link>
        </label>,
      },
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeEmail = this.handleChangeEmail.bind(this);
    this.handleChangePassword = this.handleChangePassword.bind(this);
  }
  componentWillMount() {
    if (this.props.api.LoggedIn) {
      this.redirect();
    }
  }
  redirect() {
    browserHistory.push(this.props.location.state &&
      this.props.location.state.redirectTo || '/dashboard');
  }
  notify(message, status) {
    this.setState({
      response: message,
      alertBottom: '1em',
      alertStatus: status,
    });
    setTimeout(() => this.setState({
      alertBottom: '100%',
    }), 3000);
  }
  handleSubmit(e) {
    e.preventDefault();
    if (!this.formValidate()) {
      return;
    }
    switch (this.state.view) {
      case 'login':
        this.props.api.User.login(this.state.email, this.state.password)
        .then((token) => {
          this.props.api.setAuthToken(token.token);
          this.redirect();
        })
        .catch((err) => {
          this.notify(err.message);
        });
        break;
      case 'signup':
        this.props.api.User.signup(this.state.email, this.state.password)
        .then((token) => {
          this.props.api.setAuthToken(token.token);
          this.redirect();
        })
        .catch((err) => {
          this.notify(err.message);
        });
        break;
      case 'reset':
        this.props.api.User.sendResetMail(this.state.email)
        .then((ok) => {
          this.notify('Sent recovery email', 'success');
        })
        .catch((err) => {
          this.notify(err.message);
        });
        break;
      default:
        console.log('Invalid view:', this.state.view);
    }
  }
  handleChangeEmail(e) {
    const valid = validatePassword(e.target.value);
    this.setState({
      email: e.target.value,
      email_valid: valid,
      email_label_class: e.target.value.length > 0 ? 'control-label-up' : '',
    });
  }
  handleChangePassword(e) {
    const valid = validatePassword(e.target.value);
    this.setState({
      password: e.target.value,
      password_valid: valid,
      password_label_class: e.target.value.length > 0 ? 'control-label-up' : '',
    });
  }
  formValidate() {
    console.log('view', this.state.view);
    switch (this.state.view) {
      case 'reset':
        if (!this.state.email_valid) {
          this.notify('Invalid Email');
          return false;
        }
        break;
      case 'login':
        if (!this.state.email_valid) {
          this.notify('Invalid Email');
          return false;
        }
        if (!this.state.password_valid) {
          this.notify('Invalid Password');
          return false;
        }
        break;
      case 'signup':
        console.log('hit signup');
        if (!this.state.email_valid) {
          this.notify('Invalid Email');
          return false;
        }
        if (!this.state.password_valid) {
          this.notify('Invalid Password');
          return false;
        }
        if (!this.state.termsAccepted) {
          this.notify('You need to accept the terms and conditions');
          return false;
        }
        break;
    }
    return true;
  }
  render() {
    let alertStyle = {
      position: 'fixed',
      width: '480px',
      transition: '0.5s ease all',
      marginTop: this.state.alertBottom,
    };
    console.log(alertStyle);

    let view = this.views[this.state.view];
    let passwordInput;
    if (this.state.view != 'reset') {
      passwordInput = <div className="form-group">
        <label htmlFor="password"
          className={this.state.password_label_class + ' control-label'}>
          Password
        </label>
        <input id="password" placeholder="Password" type="password"
          value={this.state.password} onChange={this.handleChangePassword}
        />
      </div>;
    }

    return (
      <div className="dashboard light">
        <div>
          <nav>
            <ul>
              <li><Link activeClassName="active" to='login'>Login</Link></li>
              <li><Link activeClassName="active" to='signup'>Sign up</Link></li>
            </ul>
            <li className="abs-tr pullRight">
              <ul><li><Link to='/'>Close</Link></li></ul>
            </li>
          </nav>
          <form onSubmit={this.handleSubmit} className="login wrapper">
            <p style={{marginBottom: '1.5em'}}>{view.introText}</p>
            <div className="form-group">
              <label htmlFor="email"
                className={this.state.email_label_class + ' control-label'}>
                Email Address
              </label>
              <input id="email" placeholder="Email Address" type="email"
                value={this.state.email} onChange={this.handleChangeEmail} />
            </div>
            { passwordInput }
            { view.note }
            <button
              className="button-primary"
              type="submit">
              {view.submitText}
            </button>
            <Alert style={alertStyle}
              status={this.state.alertStatus || 'danger'}>
              <p>{ this.state.response }</p></Alert>
          </form>
        </div>
      </div>
    );
  }
}

Login.propTypes = {
  api: PropTypes.object,
  response: PropTypes.string,
  view: PropTypes.string,
  location: PropTypes.object.isRequired,
};
