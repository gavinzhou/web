import React from 'react';
import PropTypes from 'prop-types';

const Panel = (props) => {
  const title = props.title;
  const name = encodeURIComponent(title);

  return <div className="wrapper panel" key={name}>
    <a href={'#show-'+name} className="panel-show" id={'show-'+name}>
      {title}
    </a>
    <a href={'#hide-'+name} className="panel-hide" id={'hide1-'+name}>
      {title}
    </a>

    <div className="panel-content">
      {props.children}
    </div>
  </div>;
};

export default Panel;
Panel.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.element.isRequired,
};
