import React from 'react';
import PropTypes from 'prop-types';

export default class Raw extends React.Component {
  render() {
    const className = this.props.className + ' wrapper';
    return (
      <div>
        <article>
          <div className="wrapper-wide">
            <h1>{this.props.title}</h1>
          </div>
        </article>
        <article>
          <div
            className={className}
            dangerouslySetInnerHTML={ {__html: this.props.html} }
          />
        </article>
      </div>
    );
  }
};

Raw.propTypes = {
  html: PropTypes.string.isRequired,
  className: PropTypes.string,
  title: PropTypes.string.isRequired,
};

Raw.defaultProps = {
  className: 'markdown',
};
