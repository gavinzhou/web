import React from 'react';
import PropTypes from 'prop-types';

const Tooltip = (props) => {
  return <span>
    <span className="tooltip-trigger">
      {props.children}
    </span>
    <span className="tooltip">
      {props.tip}
    </span>
  </span>;
};

export default Tooltip;
Tooltip.propTypes = {
  tip: PropTypes.element.isRequired,
  children: PropTypes.element.isRequired,
};
