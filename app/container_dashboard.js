import React from 'react';
import Gravatar from 'react-gravatar';
import PropTypes from 'prop-types';
import {Link} from 'react-router';
import {browserHistory} from 'react-router';
import {errorMessage} from './lib/messages';
import Tooltip from './components/tooltip';

export default class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tokens: [],
      vatRate: -1,
    };

    this.getUser = this.getUser.bind(this);
    this.getCard = this.getCard.bind(this);
    this.addCard = this.addCard.bind(this);
    this.deleteCard = this.deleteCard.bind(this);

    this.sendActivation = this.sendActivation.bind(this);
    this.componentWillMount = this.componentWillMount.bind(this);
    this.loadFailed = this.loadFailed.bind(this);
    this.logout = this.logout.bind(this);
    this.update = this.update.bind(this);
  }
  componentWillMount() {
    if (!this.props.api.LoggedIn) {
      browserHistory.push({
        pathname: '/login',
        state: {redirectTo: this.props.location.pathname},
      });
      return;
    }
    this.update();
  }
  update() {
    this.getUser();
    this.getCard();
    this.getSubscription();
  }
  getCard() {
    this.props.api.User.getCard()
    .then((card) => {
      this.setState({card: card});
      this.props.api.Plans.getVatRate(card.country)
      .then((vr) => {
        this.setState({
          inEU: vr.inEU,
          vatRate: vr.rate,
        });
      })
      .catch((error) => {
        this.props.notify(error);
      });
    })
    .catch((error) => {
      if (error.statusCode && error.statusCode == 404) {
        return;
      }
      this.loadFailed(err);
    });
  }
  addCard(token) {
    this.props.api.User.addCard(token)
    .then((resp) => {
      this.getCard();
    })
    .catch((err) => {
      this.loadFailed(err);
    });
  }
  deleteCard() {
    this.props.api.User.deleteCard()
    .then((resp) => {
      this.setState({
        card: null,
        inEU: undefined,
        vatRate: -1,
      });
      this.update();
    })
    .catch((err) => {
      this.loadFailed(err);
    });
  }
  getSubscription() {
    this.props.api.User.subscription()
    .then((s) => {
      let sub = s.subscription;
      if (sub) {
        sub.created = new Date(sub.created * 1000);
        sub.current_period_end = new Date(sub.current_period_end * 1000);
      }
      this.setState({subscription: sub});
    })
    .catch((error) => {
      this.loadFailed(error);
    });
  }
  getUser() {
    this.props.api.User.get()
    .then((user) => {
      this.setState({user: user});
    })
    .catch((error) => {
      this.loadFailed(error);
    });
  }
  loadFailed(error) {
    let msg = error.message;
    if (error.JWTInvalid) {
      msg = 'Session expired, please login again.';
    };
    this.props.notify(errorMessage(msg), 'danger');
    if (error.JWTInvalid) {
      browserHistory.push('/login');
    }
  }
  sendActivation() {
    this.props.api.User.sendActivationMail()
    .then(() => {
        this.props.notify('Sent activation mail. Please check your inbox.',
          'success');
    })
    .catch((error) => {
      this.loadFailed(error);
    });
  }
  logout() {
    this.props.api.removeAuthToken();
    this.setState({user: null});
    browserHistory.push('/');
  }
  render() {
    let email;
    let childs = <div className="wrapper loading">Loading dashboard...</div>;
    let activateNotice;
    if (this.state.user) {
      if (!this.state.user.activated) {
        activateNotice = <span>
          Your account is not yet activated. Please check your mail.<br />
          <a href="#" onClick={this.sendActivation}>Resent confirmation mail.
          </a></span>;
      }

      email = this.state.user.email;
      childs = React.Children.map(this.props.children, (child) => {
        return React.cloneElement(child, {
          api: this.props.api,
          update: this.update,

          notify: this.props.notify,
          sendActivation: this.sendActivation,
          loadFailed: this.loadFailed,

          user: this.state.user,
          card: this.state.card,
          vatRate: this.state.vatRate,
          inEU: this.state.inEU,

          subscription: this.state.subscription,

          addCard: this.addCard,
          deleteCard: this.deleteCard,
        });
      });
    }

    const tooltipBalance = this.state.subscription ? <span>
        Subscribed since&nbsp;
      {this.state.subscription.created.toLocaleDateString()}.<br />
        Plan renews and remaining requests expire on&nbsp;
      {this.state.subscription.current_period_end.toLocaleDateString()}
      </span> : <span>Requests expire at end of trial</span>;

    return <div className="dashboard light">
      <div>
        <header>
          <div className="abs-tl nav-user">
            <div className="gravatar">
              {email && <Gravatar email={email} />}
              <span>{email}</span>
            </div>
            <div className="notice">
              <div>{activateNotice}</div>
              <div className="balance">
                <Tooltip tip={tooltipBalance}>
                  <span>
                    {this.state.user &&
                      Number(this.state.user.balance).toLocaleString()} requests
                    remaining
                  </span>
                </Tooltip>
              </div>
            </div>
          </div>

          <nav>
             <ul>
              <li><Link activeClassName="active" to="/dashboard">Tokens</Link>
              </li>
              <li><Link activeClassName="active" to="/dashboard/plans">Plans
              </Link></li>
              <li><Link activeClassName="active" to="/dashboard/billing">
                Billing
              </Link></li>
              <li><Link activeClassName="active" to="/dashboard/account">
                Account
              </Link></li>
              <li><Link activeClassName="active" to="/dashboard/configuration">
                Config
              </Link></li>
              <li className="abs-tr">
                <ul>
                  <li>
                    <a onClick={this.logout} href="#" className="accent">
                      Log out
                    </a>
                  </li>
                </ul>
              </li>
            </ul>
          </nav>
        </header>
        {childs}
      </div>
    </div>;
  };
};
Dashboard.propTypes = {
  // Required but leading to false positives due to usage of cloneElement.
  api: PropTypes.object,
  notify: PropTypes.func,
  children: PropTypes.element.isRequired,
  location: PropTypes.object,
};
