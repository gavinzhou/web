import React from 'react';
import PropTypes from 'prop-types';

export default class Docs extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  render() {
    let childs = React.Children.map(this.props.children, (child) => {
      return React.cloneElement(child, {
        api: this.props.api,
        update: this.update,

        notify: this.props.notify,
      });
    });
    return <div className="docs light">
      {childs}
    </div>;
  };
};
Docs.propTypes = {
  // Required but leading to false positives due to usage of cloneElement.
  api: PropTypes.object,
  notify: PropTypes.func,
  children: PropTypes.element.isRequired,
};
