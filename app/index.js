let url = new URL(window.location.href);
const prerender = url.searchParams.get('prerender') == 1;

// imported via resolve
import Pace from 'pace';
if (!prerender) {
  Pace.start();
}

/* eslint-disable no-unused-vars */
import React from 'react';
/* eslint-enable no-unused-vars */
import {render} from 'react-dom';
import {
  Router,
  Route,
  IndexRoute,
  browserHistory,
  IndexRedirect,
} from 'react-router';

import Container from './container';
import ContainerPublic from './container_public';
import ContainerDashboard from './container_dashboard';
import ContainerDocs from './container_docs';

import Home from './components/home';
import Demo from './components/demo';

import Login from './components/login';
import Activate from './components/activate';

import Tokens from './components/tokens';
import Billing from './components/billing';
import Account from './components/account';
import ChangePassword from './components/change_password';
import ChangeEmail from './components/change_email';

import Plans from './components/plans';

import DocsHome from './components/docs/home';
import DocsPrometheus from './components/docs/prometheus';
import DocsDatadog from './components/docs/datadog.md';
import DocsInfluxdata from './components/docs/influxdata.md';
import DocsSensu from './components/docs/sensu.md';
import DocsZabbix from './components/docs/zabbix.md';
import DocsLibrato from './components/docs/librato.md';
import DocsCollectd from './components/docs/collectd.md';
import DocsGraphite from './components/docs/graphite.md';
import DocsSysdig from './components/docs/sysdig.md';
import DocsNagios from './components/docs/nagios.md';

import NoMatch from './components/no_match';
import Raw from './components/raw';
import Reset from './components/reset';

import LegalTerms from './markdown/legal_terms.md';
import LegalImprint from './markdown/legal_imprint.md';
import LegalPrivacy from './markdown/legal_privacy.md';
import FAQ from './components/faq';

require('./styles/app.scss');

// https://github.com/rafrex/react-router-hash-link-scroll
function hashLinkScroll() {
  const {hash} = window.location;
  if (hash !== '') {
    // Push onto callback queue so it runs after the DOM is updated,
    // this is required when navigating from a different page so that
    // the element is rendered on the page before trying to getElementById.
    setTimeout(() => {
      const id = hash.replace('#', '');
      const element = document.getElementById(id);
      if (element) {
        element.scrollIntoView();
        // Thanks https://stackoverflow.com/a/13857312/1584288
        const scrolledY = window.scrollY;
        if(scrolledY) {
            window.scroll(0, scrolledY - 75);
        }
      }
    }, 0);
  }
}

function onRouterUpdate() {
  hashLinkScroll();
  if (location.action === 'POP' || location.hash != '') {
    return;
  }
  window.scrollTo(0, 0);
}

const Signup = (props) => {
  return (
    <Login view='signup' {...props} />
  );
};

const LoginReset = (props) => {
  return (
    <Login view='reset' {...props} />
  );
};

render((
  <Router history={browserHistory} onUpdate={onRouterUpdate}>
    <Route path="/" component={Container}>
      <Route path='login' component={Login}/>
      <Route path='login/reset' component={LoginReset}/>
      <Route path='signup' component={Signup}/>
      <Route path='reset/:token' component={Reset}/>

      <Route component={ContainerPublic}>
        <IndexRoute component={Home}/>

        <Route path='demo' component={Demo}/>
        <Route path='configuration' component={DocsPrometheus}/>
        <Route path='faq' component={FAQ}/>
        <Route path='legal/terms'
          component={() => <Raw title="Term of Service" html={LegalTerms} /> }/>
        <Route path='legal/privacy'
          component={() => <Raw title="Your privacy is important to us."
            html={LegalPrivacy} /> }/>
        <Route path='legal/imprint'
          component={() => <Raw title="Imprint" html={LegalImprint} /> }/>
        <Route component={ContainerDocs}>
          <Route path='docs' component={DocsHome}/>
          <Route path='docs/prometheus' component={DocsPrometheus}/>
          <Route path='docs/datadog'
              component={() => <Raw title="Datadog" html={DocsDatadog} /> }/>/>
          <Route path='docs/influxdata' component={() =>
            <Raw title="Influxdata" html={DocsInfluxdata} /> }/>/>
          <Route path='docs/sensu'
              component={() => <Raw title="Sensu" html={DocsSensu} /> }/>/>
          <Route path='docs/zabbix'
              component={() => <Raw title="Zabbix" html={DocsZabbix} /> }/>/>
          <Route path='docs/librato'
              component={() => <Raw title="Librato" html={DocsLibrato} /> }/>/>
          <Route path='docs/collectd' component={() =>
            <Raw title="Collectd" html={DocsCollectd} /> }/>/>
          <Route path='docs/graphite' component={() =>
            <Raw title="Graphite" html={DocsGraphite} /> }/>/>
          <Route path='docs/sysdig'
              component={() => <Raw title="Sysdig" html={DocsSysdig} /> }/>/>
          <Route path='docs/nagios'
              component={() => <Raw title="Nagios" html={DocsNagios} /> }/>/>
        </Route>
      </Route>
      <Route component={ContainerDashboard}>
        <Route path='activate/:token' component={Activate}/>

        <Route path='dashboard' component={Tokens}/>
        <Route path='dashboard/plans' component={Plans}/>
        <Route path='dashboard/billing' component={Billing}/>
        <Route path='dashboard/configuration' component={DocsPrometheus}/>
        <Route path='dashboard/account' component={Account}>
          <IndexRedirect to='password'/>
          <Route path='password' component={ChangePassword}/>
          <Route path='email' component={ChangeEmail}/>
        </Route>
      </Route>
      <Route component={ContainerPublic}>
        <Route path='*' component={NoMatch}/>
      </Route>
    </Route>
  </Router>
), document.getElementById('app'));
