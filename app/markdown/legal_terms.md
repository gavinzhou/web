<p style="text-align: center; font-size: 120%">
This document is a legally binding agreement between you and Johannes Ziemke, Mittenwalder Str. 24, 10961 Berlin ("latency.at") that governs your use of Latency.at's software services.
</p>

#### 1. Terms

By accessing the website at [http://latency.at](http://latency.at), you are agreeing to be bound by these terms of service, all applicable laws and regulations, and agree that you are responsible for compliance with any applicable local laws. If you do not agree with any of these terms, you are prohibited from using or accessing this site. The materials contained in this website are protected by applicable copyright and trademark law.

#### 2. Use License
Unless explicitly stated otherwise, permission is granted to temporarily download one copy of the materials (information or software) on latency.at's website for transitory viewing only. This is the grant of a license, not a transfer of title, and under this license you may not:

1.  modify or copy the materials;
2.  attempt to decompile or reverse engineer any software contained on latency.at's website;
3.  remove any copyright or other proprietary notations from the materials; or
4.  transfer the materials to another person or "mirror" the materials on any other server.

This license shall automatically terminate if you violate any of these restrictions and may be terminated by latency.at at any time. Upon terminating your viewing of these materials or upon the termination of this license, you must destroy any downloaded materials in your possession whether in electronic or printed format.

#### 3. Disclaimer
The materials and services on latency.at's website are provided on an 'as is' basis. latency.at makes no warranties, expressed or implied, and hereby disclaims and negates all other warranties including, without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, or non-infringement of intellectual property or other violation of rights.
Further, latency.at does not warrant or make any representations concerning the accuracy, likely results, or reliability of the use of the materials and services on its website or otherwise relating to such materials and services or on any sites linked to this site.
In no event shall latency.at or its suppliers be liable for any damages (including, without limitation, damages for loss of data or profit, or due to business interruption) arising out of the use or inability to use the materials or services on latency.at's website, even if latency.at or a latency.at authorized representative has been notified orally or in writing of the possibility of such damage. Because some jurisdictions do not allow limitations on implied warranties, or limitations of liability for consequential or incidental damages, these limitations may not apply to you.

#### 4. Use of Services
Offers published by latency.at on the Website are non-binding.
The User may use the Services within certain limits free of charge until the trial requests expire one month after registration.
Furthermore, Users may use latency.at’s Services upon subscribing for a Plan. A Plan includes a certain number of request to be used within the subscription cycle as further described on the Website.
By placing an order by clicking "Upgrade" (= "Purchase"), the User makes a binding offer to use the respective Services within a Plan. The offer shall be deemed to be accepted by latency.at either upon e-mail acceptance of the offer or by making available the respective Service. Upon the acceptance of a purchase, a contract between the User and latency.at is concluded.

#### 5. Technical Requirements
In order to consume the data provided by the service a Prometheus server is required.
The User is solely responsible for the fulfilment of the Technical Requirements; latency.at does not owe and/or provide any consultancy services in connection therewith.

#### 6. Registration
Any use of the Services requires prior registration on the Website by creating a latency.at account and acceptance of these Terms of Service. By clicking the button containing the words "Sign up" a User agrees he has read, understood, and accepted the Terms of Service.
latency.at offers its Services to persons over 18 years of age.
The User is responsible for the accuracy of the information necessary for registration as a User. He is obligated to keep passwords secret and protect it against unauthorised use by third parties. In case of misuse or loss of the password or in case of an appropriate suspicion, he must report the same to support@latency.at.

#### 7. Cancellation and Termination
Verbal, physical, written or other abuse (including threats of abuse) of any latency.at customer, employee, member, or officer will result in immediate account termination.
We reserve the right to modify or terminate your Service for any reason, without notice at any time.

#### 8. Modifications
latency.at may revise these terms of service for its website at any time without notice. By using this website you are agreeing to be bound by the then current version of these terms of service. You can review the most current version of the Terms of Service at: https://latency.at/legal/terms

#### 9. Indemnification
A User will indemnify, defend, and hold harmless latency.at and its officers, directors, employees for any and all claims, suits, litigation, causes of action, losses, damages, expenses, costs (including court costs and attorneys’ fees) and liabilities ("Losses") that arise out of, or in connection with (i) the Users use of the Website and/or Services; (ii) any breach by User of any warranty defined in Section 8; (iii) any claim that the User’s Content caused damage to a third party.
In cases of an aforementioned enforcement of claims by third parties, the User will provide latency.at with all the information that is needed for the examination of the claim and for the defence against it. The User provides the information immediately, truthfully, and completely.

#### 10. Miscellaneous
Any contracts entered into between latency.at and a User shall be governed by the laws of the Federal Republic of Germany under exclusion of the UN Convention on the International Sale of Goods (CISG), without prejudice to any mandatory conflict of laws provisions.
If the User is a Business Client or if the User is a legal entity or a foreign special fund organized under public law, the courts in Berlin, Germany, shall have exclusive jurisdiction in respect of all disputes arising out of or in connection with the relevant contract.
Should any provision of this Terms of Service be or become invalid, ineffective or unenforceable as a whole or in part, the validity, effectiveness and enforceability of the remaining provisions shall not be affected thereby. Any such invalid, ineffective or unenforceable provisions shall be deemed replaced by such valid, effective and enforceable provision as come closest to the economic intent and purpose as of such invalid, ineffective or unenforceable provisions as regard subject-matter, amount, time, place and extent. The aforesaid shall apply mutatis mutandis to any gap in these Terms of Service if any court has confirmed such proceeding.
latency.at has not reviewed all of the sites linked to its website and is not responsible for the contents of any such linked site. The inclusion of any link does not imply endorsement by latency.at of the site. Use of any such linked website is at the user's own risk.
