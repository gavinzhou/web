const prodDomains = [
  'latency.at',
  'dev.latency.at',
];

let env = 'dev';

if (prodDomains.find((e) => {
    return e == window.location.hostname;
  })) {
  env = 'prod';
}

let url = new URL(window.location.href);

const commonConfig = {
  'env': env,
  'minCommitment': 100000,
  'headers': {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  },
  'gaID': 'UA-22856766-5',
  'sentryURL': 'https://3ab9a9518754431cab936aca83bbcbe5@sentry.io/191716',
	'prerender': url.searchParams.get('prerender') == 1,
  'pixelID': 284231225397013,
};

const envConfig = {
  'prod': {
    'stripeKey': 'pk_live_6CTBRUrCvYpYNp2oqPWnF39K',
    'api': 'https://api.latency.at/api',
  },
  'dev': {
    'stripeKey': 'pk_test_bFipZSJ4PDRJGMkYmMGnyCvx',
    'api': 'http://' + window.location.hostname + ':8000/api',
  },
};

export default Object.assign(commonConfig, envConfig[env]);
